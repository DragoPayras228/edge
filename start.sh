
## This script uses: sh, echo, app_process, mkdir, cd, touch, ln, unzip, sleep, rm00118828283

if [ ! -z "$1" ]; then
    export APP_TARGET_EXECUTABLE="$1"
fi

export LOG_PATH="/sdcard/moboxedge"

echo "Starting xserver"

/system/bin/app_process / $APP_PACKAGE_NAME.CmdEntryPoint :0 &>"$LOG_PATH/x11-server-log.txt" &

echo "Loading exported settings from the app"

source "$APP_FILES_DIR/usr/mobox/exported-settings.sh"

if [ -f "$APP_FILES_DIR/usr/wineprefix/$APP_SELECTED_CONTAINER/mobox-settings/selected-preset" ]; then
    APP_SELECTED_PRESET="$(cat $APP_FILES_DIR/usr/wineprefix/$APP_SELECTED_CONTAINER/mobox-settings/selected-preset)"

    if [ ! "$APP_SELECTED_PRESET" = "" ] && [ ! "$APP_SELECTED_PRESET" = "global" ]; then
        echo "Switching to $APP_SELECTED_PRESET environment preset"
        source "$APP_FILES_DIR/usr/mobox/exported-settings-$APP_SELECTED_PRESET.sh"
    fi
    unset APP_SELECTED_PRESET
fi

export APP_SELECTED_WINE="$(cat $APP_FILES_DIR/usr/wineprefix/$APP_SELECTED_CONTAINER/used-wine)"

export LC_ALL="$LANG"
export PULSE_LATENCY_MSEC=100
export WINE_CPU_TOPOLOGY=4:4,5,6,7
export DISPLAY=:0
export BOX64_MMAP32=1
export WINEARCH=win64
export WINEESYNC_TERMUX=1
export GALLIUM_HUD_PERIOD=1
export tu_allow_oob_indirect_ubo_loads=true
export MESA_VK_WSI_PRESENT_MODE=mailbox

export PREFIX="$APP_FILES_DIR/usr"
export HOME="$APP_FILES_DIR/home"
export TMPDIR="$PREFIX/tmp"
export GLIBC_PREFIX="$PREFIX/glibc"
export SHELL="$(which sh)"
export PATH="$PATH:$GLIBC_PREFIX/bin"

export FONTCONFIG_PATH="$GLIBC_PREFIX/etc/fonts"
export VK_ICD_FILENAMES="$GLIBC_PREFIX/etc/vulkan/icd.d/freedreno_icd.aarch64.json"
export VK_DRIVER_FILES="$VK_ICD_FILENAMES"
export DXVK_CONFIG_FILE="$GLIBC_PREFIX/share/dxvk.conf"

export WINE_PATH="$PREFIX/wine/$APP_SELECTED_WINE"
export WINEPREFIX="$PREFIX/wineprefix/$APP_SELECTED_CONTAINER"
export BOX64_LD_LIBRARY_PATH="$GLIBC_PREFIX/lib/x86_64-linux-gnu:$WINE_PATH/lib/wine/x86_64-unix"
export BOX64_RCFILE="$GLIBC_PREFIX/etc/box64.box64rc"

##export BOX64_CPUNAME=$(lscpu | grep "Model name:" | sed -r 's/Model name:\ {1,}//g' | cut -d $'\n' -f 1)

export GALLIUM_DRIVER=zink
export MESA_LOADER_DRIVER_OVERRIDE=zink
export LIBGL_DRIVERS_PATH="$GLIBC_PREFIX/lib/dri"

mkdir -p "$GLIBC_PREFIX/bin"
mkdir -p "$GLIBC_PREFIX/share"
mkdir -p "$GLIBC_PREFIX/etc"
mkdir -p "$GLIBC_PREFIX/lib/x86_64-linux-gnu"
mkdir -p "$HOME"
cd "$HOME"
mkdir -p "$TMPDIR"
mkdir -p "$PREFIX/wine"
mkdir -p "$PREFIX/wineprefix"

if [ -z "$APP_TARGET_EXECUTABLE" ]; then
    export APP_TARGET_EXECUTABLE="$PREFIX/mobox/apps/tfm.exe"
    export APP_TARGET_EXECUTABLE="$PREFIX/mobox/apps/explorer.exe"
fi


echo "Target executable: $APP_TARGET_EXECUTABLE"

# generate locales
if [ ! -f "$GLIBC_PREFIX/lib/locale/mobox-locales-generated" ]; then
    echo "Generating locales"
    rm -rf "$GLIBC_PREFIX/lib/locale"
    mkdir -p "$GLIBC_PREFIX/lib/locale"

    "$GLIBC_PREFIX/bin/localedef" --no-archive -i ru_RU -c -f ISO-8859-5 -A "$GLIBC_PREFIX/share/locale/locale.alias" "$GLIBC_PREFIX/lib/locale/ru_RU" >"$LOG_PATH/localedef.txt" 2>&1
    "$GLIBC_PREFIX/bin/localedef" --no-archive -i ru_RU -c -f UTF-8 -A "$GLIBC_PREFIX/share/locale/locale.alias" "$GLIBC_PREFIX/lib/locale/ru_RU.UTF-8" >>"$LOG_PATH/localedef.txt" 2>&1
    "$GLIBC_PREFIX/bin/localedef" --no-archive -i en_US -c -f UTF-8 -A "$GLIBC_PREFIX/share/locale/locale.alias" "$GLIBC_PREFIX/lib/locale/en_US.UTF-8" >>"$LOG_PATH/localedef.txt" 2>&1
    "$GLIBC_PREFIX/bin/localedef" --no-archive -i en_US -c -f ISO-8859-1 -A "$GLIBC_PREFIX/share/locale/locale.alias" "$GLIBC_PREFIX/lib/locale/en_US" >>"$LOG_PATH/localedef.txt" 2>&1
    touch "$GLIBC_PREFIX/lib/locale/mobox-locales-generated"
fi


if [ -d "$TMPDIR" ]; then
    rm -rf "$TMPDIR/"*
fi

if [ ! -e "$GLIBC_PREFIX/etc/passwd" ]; then
    echo "Generating glibc passwd user"
    x=$(/system/bin/id -u)
    echo "u0_a${x:2}::$x:$x::$HOME:$SHELL">"$GLIBC_PREFIX/etc/passwd"
    unset x
fi

mkdir -p "$LOG_PATH/trace"
rm -rf "$LOG_PATH/trace/"*

ln -sf "$WINE_PATH/bin/wine" "$GLIBC_PREFIX/bin/wine"
ln -sf "$WINE_PATH/bin/wineserver" "$GLIBC_PREFIX/bin/wineserver"

echo "Starting pulse server"

chmod +x "$PREFIX/mobox/pulse/pulseaudio"
LD_LIBRARY_PATH=$PREFIX/mobox/pulse \
 TMPDIR=$PREFIX/tmp \
 $PREFIX/mobox/pulse/pulseaudio \
  --disable-shm=true \
  --system=false \
  --daemonize=false \
  --use-pid-file=false \
  -n --file="$PREFIX/mobox/pulse/default.pa" \
  --exit-idle-time=-1 --fail=false \
   >"$LOG_PATH/pulseaudio.txt" 2>&1 &

chmod +x "$GLIBC_PREFIX/bin/box64"

echo "Processing configuration variables"

case "$ENABLE_BOX64_DEBUG" in
2)
    export BOX64_LOG=1
    export BOX64_NOBANNER=0
    export BOX64_SHOWSEGV=1
    export BOX64_DYNAREC_MISSING=1
    export BOX64_DLSYM_ERROR=1
;;
1)
    export BOX64_LOG=1
    export BOX64_NOBANNER=0
    export BOX64_SHOWSEGV=1
    export BOX64_DYNAREC_MISSING=1
    export BOX64_DLSYM_ERROR=0
;;
*)
    export BOX64_LOG=0
    export BOX64_NOBANNER=1
    export BOX64_SHOWSEGV=0
    export BOX64_DYNAREC_MISSING=0
    export BOX64_DLSYM_ERROR=0
;;
esac
unset ENABLE_BOX64_DEBUG


case "$ENABLE_WINE_DEBUG" in
1)
    unset WINEDEBUG
;;
2)
    export WINEDEBUG=warn+all
;;
3)
    export WINEDEBUG=+all
;;
*)
    export WINEDEBUG=-all
;;
esac
unset ENABLE_WINE_DEBUG


case "$ENABLE_MESA_DEBUG" in
2)
    unset MESA_NO_ERROR
    export LIBGL_DEBUG=verbose
    export VK_LOADER_DEBUG=all
;;
1)
    unset MESA_NO_ERROR
    export LIBGL_DEBUG=1
    export VK_LOADER_DEBUG=error,warn
;;
*)
    export MESA_NO_ERROR=1
    unset LIBGL_DEBUG
    unset VK_LOADER_DEBUG
;;
esac
unset ENABLE_MESA_DEBUG


if [ "$ENABLE_BOX64_TRACE_PER_PID" = "1" ]; then
    export BOX64_TRACE_FILE="$LOG_PATH/trace/trace-%pid.txt"
fi
unset ENABLE_BOX64_TRACE_PER_PID

export MANGOHUD=0
if [ "$MANGOHUD_OPTION_ENABLE" = "1" ]; then
    export MANGOHUD=1
fi
unset MANGOHUD_OPTION_ENABLE

export MANGOHUD_CONFIG="fps=0,frame_timing=0,cpu_stats=0,gpu_stats=0,no_small_font"

if [ "$MANGOHUD_OPTION_BATTERY" = "1" ]; then
    MANGOHUD_CONFIG+=",battery,battery_watt"
fi
unset MANGOHUD_OPTION_BATTERY

if [ "$MANGOHUD_OPTION_CPU_INFO" = "1" ]; then
    MANGOHUD_CONFIG+=",core_load,cpu_mhz,cpu_stats"
fi
unset MANGOHUD_OPTION_CPU_INFO

if [ "$MANGOHUD_OPTION_GPU_INFO" = "1" ]; then
    MANGOHUD_CONFIG+=",gpu_stats,gpu_temp"
fi
unset MANGOHUD_OPTION_GPU_INFO

if [ "$MANGOHUD_OPTION_RAM" = "1" ]; then
    MANGOHUD_CONFIG+=",ram,swap"
fi
unset MANGOHUD_OPTION_RAM

if [ "$MANGOHUD_OPTION_ENGINE" = "1" ]; then
    MANGOHUD_CONFIG+=",gpu_name,arch,vulkan_driver,engine_version"
fi
unset MANGOHUD_OPTION_ENGINE

if [ "$MANGOHUD_OPTION_RESOLUTION_ARCH" = "1" ]; then
    MANGOHUD_CONFIG+=",resolution"
fi
unset MANGOHUD_OPTION_RESOLUTION_ARCH

if [ "$MANGOHUD_OPTION_FPS" = "1" ]; then
    MANGOHUD_CONFIG+=",fps"
fi
unset MANGOHUD_OPTION_FPS

if [ "$MANGOHUD_OPTION_FRAMETIME" = "1" ]; then
    MANGOHUD_CONFIG+=",frame_timing"
fi
unset MANGOHUD_OPTION_FRAMETIME

if [ ! "$MANGOHUD_OPTION_FONT_SIZE" = "" ]; then
    MANGOHUD_CONFIG+=",font_size=$MANGOHUD_OPTION_FONT_SIZE"
fi
unset MANGOHUD_OPTION_FONT_SIZE

if [ ! "$MANGOHUD_OPTION_OPACITY" = "" ]; then
    MANGOHUD_CONFIG+=",alpha=$MANGOHUD_OPTION_OPACITY"
fi
unset MANGOHUD_OPTION_OPACITY

if [ ! "$MANGOHUD_OPTION_BACKGROUND_OPACITY" = "" ]; then
    MANGOHUD_CONFIG+=",background_alpha=$MANGOHUD_OPTION_BACKGROUND_OPACITY"
fi
unset MANGOHUD_OPTION_BACKGROUND_OPACITY

if [ ! "$MANGOHUD_OPTION_POSITION" = "" ]; then
    MANGOHUD_CONFIG+=",position=$MANGOHUD_OPTION_POSITION"
fi
unset MANGOHUD_OPTION_POSITION


export DXVK_HUD=""

if [ "$DXVK_HUD_DEVINFO" = "1" ]; then
    DXVK_HUD+="devinfo"
fi
unset DXVK_HUD_DEVINFO

if [ "$DXVK_HUD_FPS" = "1" ]; then
    if [ ! "$DXVK_HUD" = "" ]; then
        DXVK_HUD+=","
    fi
    DXVK_HUD+="fps"
fi
unset DXVK_HUD_FPS

if [ "$DXVK_HUD_FRAMETIMES" = "1" ]; then
    if [ ! "$DXVK_HUD" = "" ]; then
        DXVK_HUD+=","
    fi
    DXVK_HUD+="frametimes"
fi
unset DXVK_HUD_FRAMETIMES

if [ "$DXVK_HUD_PIPELINES" = "1" ]; then
    if [ ! "$DXVK_HUD" = "" ]; then
        DXVK_HUD+=","
    fi
    DXVK_HUD+="submissions,drawcalls,pipelines,descriptors"
fi
unset DXVK_HUD_PIPELINES

if [ "$DXVK_HUD_MEMORY" = "1" ]; then
    if [ ! "$DXVK_HUD" = "" ]; then
        DXVK_HUD+=","
    fi
    DXVK_HUD+="memory"
fi
unset DXVK_HUD_MEMORY

if [ "$DXVK_HUD_GPULOAD" = "1" ]; then
    if [ ! "$DXVK_HUD" = "" ]; then
        DXVK_HUD+=","
    fi
    DXVK_HUD+="gpuload"
fi
unset DXVK_HUD_GPULOAD

if [ "$DXVK_HUD_VERSION" = "1" ]; then
    if [ ! "$DXVK_HUD" = "" ]; then
        DXVK_HUD+=","
    fi
    DXVK_HUD+="version"
fi
unset DXVK_HUD_VERSION

if [ "$DXVK_HUD_API" = "1" ]; then
    if [ ! "$DXVK_HUD" = "" ]; then
        DXVK_HUD+=","
    fi
    DXVK_HUD+="api"
fi
unset DXVK_HUD_API

if [ ! "$DXVK_HUD_SCALE" = "" ]; then
    if [ ! "$DXVK_HUD" = "" ]; then
        DXVK_HUD+=","
    fi
    DXVK_HUD+="scale=$DXVK_HUD_SCALE"
fi
unset DXVK_HUD_SCALE

if [ ! "$DXVK_HUD_OPACITY" = "" ]; then
    if [ ! "$DXVK_HUD" = "" ]; then
        DXVK_HUD+=","
    fi
    DXVK_HUD+="opacity=$DXVK_HUD_OPACITY"
fi
unset DXVK_HUD_OPACITY

if [ "$DXVK_HUD_ENABLED" = "0" ]; then
    unset DXVK_HUD
fi

export TU_DEBUG="noconform"
if [ "$TU_DEBUG_SYNCDRAW" = "1" ]; then
    TU_DEBUG+=",syncdraw"
fi
unset TU_DEBUG_SYNCDRAW
if [ "$TU_DEBUG_FLUSHALL" = "1" ]; then
    TU_DEBUG+=",flushall"
fi
unset TU_DEBUG_FLUSHALL

if [ "$MESA_VK_WSI_DEBUG_SW" = "1" ]; then
    export MESA_VK_WSI_DEBUG=sw
fi
unset MESA_VK_WSI_DEBUG_SW

if [ -e "$WINEPREFIX/mobox-settings/apply-on-startup-box64" ]; then
    unzip -o "$PREFIX/mobox/drivers/box64-$(cat $WINEPREFIX/mobox-settings/selected-box64).zip" -d "$APP_FILES_DIR"
    rm -rf "$WINEPREFIX/mobox-settings/apply-on-startup-box64"
fi

if [ -e "$WINEPREFIX/mobox-settings/apply-on-startup-turnip" ]; then
    unzip -o "$PREFIX/mobox/drivers/turnip-$(cat $WINEPREFIX/mobox-settings/selected-turnip).zip" -d "$APP_FILES_DIR"
    rm -rf "$WINEPREFIX/mobox-settings/apply-on-startup-turnip"
fi

if [ ! -f "$WINEPREFIX/creation-finished" ]; then
   export TEMP_WINEESYNC="$WINEESYNC"
   export TEMP_SAFEFLAGS="$BOX64_DYNAREC_SAFEFLAGS"
   export TEMP_BIGBLOCK="$BOX64_DYNAREC_BIGBLOCK"
   unset WINEESYNC
   unset BOX64_DYNAREC_SAFEFLAGS
   unset BOX64_DYNAREC_BIGBLOCK
   echo "Creating wine prefix"
   WINEDLLOVERRIDES="winegstreamer,mscoree=" \
    box64 wine \
     wineboot -u \
      >"$LOG_PATH/wineboot.txt" 2>&1
   if [ ! -f "$WINEPREFIX/.update-timestamp" ]; then
       echo "Wine prefix creation failed"
       return 1
   fi
   echo "$APP_SELECTED_WINE">"$WINEPREFIX/used-wine"
   echo "disable">"$WINEPREFIX/.update-timestamp"
   echo "Installing some drive_c patches"
   mkdir -p "$WINEPREFIX/mobox-settings"
   unzip -o "$PREFIX/mobox/drivers/drive_c_patches.zip" -d "$WINEPREFIX/drive_c"
   echo "Installing dosdevices symlinks"
   rm -rf "$WINEPREFIX/dosdevices/z:"
   rm -rf "$WINEPREFIX/dosdevices/e:"
   ln -sf "$PREFIX" "$WINEPREFIX/dosdevices/z:"
   ln -sf "/sdcard/Download" "$WINEPREFIX/dosdevices/d:"
   ln -sf $(df -H | grep -o "/storage/....-....") "$WINEPREFIX/dosdevices/f:"
   ln -sf "/sdcard/Android/data/$APP_PACKAGE_NAME/files/Download" "$WINEPREFIX/dosdevices/e:"
   echo "Applying registry patches"
   for i in user.reg system.reg disable_ndis.reg wide_buttons.reg; do
       if [ ! -f "$PREFIX/mobox/drivers/$i" ]; then continue; fi
       box64 wine regedit "$PREFIX/mobox/drivers/$i" &>/dev/null
   done
   echo "Installing programs and redistributables"
   box64 wine start /wait Z:\\mobox\\apps\\first-install.bat &>/dev/null

   sleep 15
   touch "$WINEPREFIX/creation-finished"
   box64 wineserver -k
   export WINEESYNC="$TEMP_WINEESYNC"
   export BOX64_DYNAREC_SAFEFLAGS="$TEMP_SAFEFLAGS"
   export BOX64_DYNAREC_BIGBLOCK="$TEMP_BIGBLOCK"
   unset TEMP_WINEESYNC
   unset TEMP_SAFEFLAGS
   unset TEMP_BIGBLOCK
   sleep 1
fi

echo "Installing drivers"
if [ -e "$WINEPREFIX/mobox-settings/apply-on-startup-dxvk" ]; then
    unzip -o "$PREFIX/mobox/drivers/dxvk-$(cat $WINEPREFIX/mobox-settings/selected-dxvk).zip" -d "$WINEPREFIX/drive_c"
    rm -rf "$WINEPREFIX/mobox-settings/apply-on-startup-dxvk"
fi
if [ -e "$WINEPREFIX/mobox-settings/apply-on-startup-vkd3d" ]; then
    unzip -o "$PREFIX/mobox/drivers/vkd3d-$(cat $WINEPREFIX/mobox-settings/selected-vkd3d).zip" -d "$WINEPREFIX/drive_c"
    rm -rf "$WINEPREFIX/mobox-settings/apply-on-startup-vkd3d"
fi
if [ -e "$WINEPREFIX/mobox-settings/apply-on-startup-d8vk" ]; then
    unzip -o "$PREFIX/mobox/drivers/d8vk-$(cat $WINEPREFIX/mobox-settings/selected-d8vk).zip" -d "$WINEPREFIX/drive_c"
    rm -rf "$WINEPREFIX/mobox-settings/apply-on-startup-d8vk"
fi
if [ -e "$WINEPREFIX/mobox-settings/apply-on-startup-directx-dlls" ]; then
    unzip -o "$PREFIX/mobox/drivers/directx-dlls-$(cat $WINEPREFIX/mobox-settings/selected-directx-dlls).zip" -d "$WINEPREFIX/drive_c"
    rm -rf "$WINEPREFIX/mobox-settings/apply-on-startup-directx-dlls"
fi


echo "Starting main executable"

touch "$PREFIX/mobox/startup-complete"

box64 wine explorer /desktop=shell,$RESOLUTION \
 "$APP_TARGET_EXECUTABLE" \
  >"$LOG_PATH/main.txt" 2>&1

box64 wineserver -k